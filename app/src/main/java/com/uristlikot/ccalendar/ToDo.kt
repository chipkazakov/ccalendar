package com.uristlikot.ccalendar


import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_to_do.*


class ToDo : AppCompatActivity() {
    var dbHandler: DatabaseHandler? = null
    var dbHandlerWork: DatabaseWorkDays? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_do)
        dbHandler = DatabaseHandler(this)
        dbHandlerWork = DatabaseWorkDays(this)
        val txFromMain = intent.getStringExtra(EXTRA_TASK_DESCRIPTION)
        fun validation(): Boolean {
            var validate = false

            if (!editText_firstName.text.toString().equals("")) {
                validate = true
            } else {
                validate = false
                val toast = Toast.makeText(this, "Fill all details", Toast.LENGTH_LONG).show()
            }

            return validate
        }

        val userd = dbHandler!!.getAllUsers(txFromMain.toString()).split("\n")

        val lst = mutableListOf<String>()
        for (i in userd) {
            lst.add(i)
        }
        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1, lst
        )

        // присваиваем адаптер списку
        lstView.setAdapter(adapter)
        lstView.setOnItemLongClickListener { parent, view, position, id ->
            dbHandler!!.delete(lstView.getItemAtPosition(position).toString())
            Toast.makeText(this,getString(R.string.deleted),Toast.LENGTH_SHORT).show()
            true
        }


        //on Click Save button
        button_save.setOnClickListener(View.OnClickListener {
            // checking input text should not be null
            if (validation()) {
                val user: Users = Users()
                var success: Boolean = false
                user.firstName = editText_firstName.text.toString()
                user.date = txFromMain.toString()

                success = dbHandler!!.addUser(user)

                if (success) {
                    val toast = Toast.makeText(this, "Saved Successfully", Toast.LENGTH_LONG).show()
                }
            }
            val userd = dbHandler!!.getAllUsers(txFromMain.toString()).split(" ")
            val lst = mutableListOf<String>()
            for (i in userd) {
                lst.add(i)
            }
            val adapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1, lst
            )

            // присваиваем адаптер списку
            lstView.setAdapter(adapter)
        })

        buttonBack.setOnClickListener {
            onBackPressed()
        }

        //on Click show button

        //actionbar


    }

    override fun onBackPressed() {
        super.onBackPressed()
        getTimerLength(this)
    }

    fun getTimerLength(context: Context): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        if (preferences.getBoolean(CheckBoxValue, false) == false) {
            val date = intent.getStringExtra(EXTRA_TASK_DESCRIPTION)
            dbHandlerWork!!.delete(date)
        } else {
            val date = intent.getStringExtra(EXTRA_TASK_DESCRIPTION)
            dbHandlerWork!!.addWorkDay(date)
        }

        return preferences.getBoolean(CheckBoxValue, false)
    }

    companion object {
        const val EXTRA_TASK_DESCRIPTION = "date"
        const val CheckBoxValue = "com.uristlikot.checkbox3"


    }

}

class ToDoActivityFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.todo_pref)
    }
}




