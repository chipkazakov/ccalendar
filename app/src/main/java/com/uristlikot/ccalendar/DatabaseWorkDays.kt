package com.uristlikot.ccalendar

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseWorkDays(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSIOM) {
    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME " +
                "($ID Integer PRIMARY KEY, $ChckBox TEXT, $DATE TEXT)"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Called when the database needs to be upgraded
    }

    fun addWorkDay(date: String): Boolean {
        //Create and/or open a database that will be used for reading and writing.
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(DATE, date)
        val _success = db.insert(TABLE_NAME, null, values)
        db.close()
        return (Integer.parseInt("$_success") != -1)
    }

    fun getAllWorkDays(): String {
        val db = readableDatabase

        var allDates: String = ""
        val selectALLQuery = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val date = cursor.getString(cursor.getColumnIndex(DATE))


                    allDates = "$allDates$date\n"

                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allDates
    }

    fun delete(productname: String): String {


        var result = false

        val query =
            "SELECT * FROM $TABLE_NAME WHERE $DATE = $productname"

        val db = this.writableDatabase

        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(
                TABLE_NAME, "$ID = ?",
                arrayOf(id.toString())
            )
            cursor.close()
            result = true
        }
        db.execSQL("delete from " + TABLE_NAME +" where $DATE=\"$productname\"")
        db.close()
        return result.toString()
    }

    companion object {
        private val DB_NAME = "WorkDB"
        private val ChckBox = "ChckBox"
        private val DB_VERSIOM = 1
        private val TABLE_NAME = "dates"
        private val ID = "id"
        private val COLUMN_ID = "_id"
        private val DATE = "Date"

    }
}