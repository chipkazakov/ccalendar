package com.uristlikot.ccalendar
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_help.*


class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        buttonBack1.setOnClickListener {
            onBackPressed()
        }

        val string=getString(R.string.version)+": "+BuildConfig.VERSION_NAME+"."+"28"
        textView2.setText(
            string
        )

    }
}
