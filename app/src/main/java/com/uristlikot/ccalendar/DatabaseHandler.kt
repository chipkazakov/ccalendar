package com.uristlikot.ccalendar


import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DatabaseHandler(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSIOM) {

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME " +
                "($ID Integer PRIMARY KEY, $FIRST_NAME TEXT, $DATE TEXT)"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Called when the database needs to be upgraded
    }

    //Inserting (Creating) data
    fun addUser(user: Users): Boolean {
        //Create and/or open a database that will be used for reading and writing.
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(FIRST_NAME, user.firstName)
        values.put(DATE, user.date)
        val _success = db.insert(TABLE_NAME, null, values)
        db.close()
        Log.v("InsertedID", "$_success")
        return (Integer.parseInt("$_success") != -1)
    }

    //get all users
    fun getAllUsers(string1: String): String {
        var allUser: String = ""
        val txFromMain = string1
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    if (txFromMain == cursor.getString(cursor.getColumnIndex(DATE))) {
                        val id = cursor.getString(cursor.getColumnIndex(ID))
                        val firstName = cursor.getString(cursor.getColumnIndex(FIRST_NAME))
                        val date = cursor.getString(cursor.getColumnIndex(DATE))

                        allUser = "$allUser\n$firstName"
                    }

                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allUser
    }

    fun getAllDates(): String {
        var allDates: String = ""
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val date = cursor.getString(cursor.getColumnIndex(DATE))

                    allDates = "$allDates\n$date"

                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allDates
    }
    fun delete(productname: String): String {


        var result = false



        val db = this.writableDatabase



        db.execSQL("delete from " + TABLE_NAME +" where $FIRST_NAME=\"$productname\"")
        db.close()
        return result.toString()
    }
    companion object {
        private val DB_NAME = "UsersDB"
        private val ChckBox = "ChckBox"
        private val DB_VERSIOM = 1;
        private val TABLE_NAME = "users"
        private val ID = "id"
        private val FIRST_NAME = "FirstName"
        private val DATE = "Date"

    }
}