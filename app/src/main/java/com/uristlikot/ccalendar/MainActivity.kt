package com.uristlikot.ccalendar


import android.app.DatePickerDialog
import android.app.Dialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import com.uristlikot.ccalendar.ToDo
import kotlinx.android.synthetic.main.activity_main.*
import ru.cleverpumpkin.calendar.CalendarDate
import ru.cleverpumpkin.calendar.CalendarView
import ru.cleverpumpkin.calendar.utils.getColorInt
import java.text.SimpleDateFormat
import java.util.*




interface DateIndicator {
    val date: CalendarDate // indicator date
    val color: Int // indicator color
}

var dbHandlerWork: DatabaseWorkDays? = null
var datee: MutableList<CalendarDate>? = null
var yearFrom: Int? = null
var monthFrom: Int? = null
var dayFrom: Int? = null

class MainActivity : AppCompatActivity() {
    class CalendarDateIndicator(
        override val date: CalendarDate,
        override val color: Int,
        val eventName: String

    ) : CalendarView.DateIndicator

    class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            // Use the current date as the default date in the picker
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            // Create a new instance of DatePickerDialog and return it
            return DatePickerDialog(activity, this, year, month, day)
        }

        override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
            dayFrom = day
            monthFrom = month
            yearFrom = year
            (activity as MainActivity).resFrom()
        }
    }

    var dbHandler: DatabaseHandler? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dbHandlerWork = DatabaseWorkDays(this)
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.getInstance().get(2), 1)
        val minDate = CalendarDate(calendar.time)
// Initial date

        calendar.set(2019, Calendar.getInstance().get(2), 1)
        val initialDate = CalendarDate(calendar.time)

// Minimum available date

        val dates: MutableList<CalendarDate> = mutableListOf()
        calendar.add(Calendar.DAY_OF_MONTH, -2)
        for (g in 1..100) {
            if (g % 2 == 1) {
                calendar.add(Calendar.DAY_OF_MONTH, 3)
                dates.add(CalendarDate(calendar.time))
                calendar.add(Calendar.DAY_OF_MONTH, 1)
                dates.add(CalendarDate(calendar.time))
            }

        }
//
        val preselectedDates: List<CalendarDate> = dates

// Maximum available date
        calendar.set(2020, Calendar.getInstance().get(2) + 1, 1)
        val maxDate = CalendarDate(calendar.time)


// List of preselected dates that will be initially selected

// The first day of week
        val firstDayOfWeek = java.util.Calendar.MONDAY

// Set up calendar with all available parameters
        calendar_view.setupCalendar(
            initialDate = initialDate,
            minDate = minDate,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.MULTIPLE,
            selectedDates = preselectedDates,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = false
        )




        calendar_view.onDateClickListener = { date ->
            val selectedDates = date
            val randomIntent = Intent(this, ToDo::class.java)
            val countString = selectedDates.toString()
            randomIntent.putExtra(ToDo.EXTRA_TASK_DESCRIPTION, countString)
            startActivity(randomIntent)
        }





        calendar_view.onDateLongClickListener = { date ->


        }
        dbHandler = DatabaseHandler(this)
        val userss = dbHandler!!.getAllDates().replace("\n", " ")
        val lstValues: List<String> = userss.split(" ").map { it -> it.trim() }
        for (i in lstValues) {
            val sdf = SimpleDateFormat("dd/M/yyyy")
            val currentDate = sdf.format(Date())

            if (i == currentDate) {
                notification(currentDate)


            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.header_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.help -> {
            intent = Intent(this, HelpActivity::class.java)
            startActivity(intent)
            true
        }
        R.id.changedate -> {
            val newFragment = DatePickerFragment()
            newFragment.show(supportFragmentManager, "datePicker")
            val year = newFragment.arguments?.getString("year")
            println(yearFrom)
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        res()
        val indicators = mutableListOf<CalendarView.DateIndicator>()
        val calendar = Calendar.getInstance()
        dbHandler = DatabaseHandler(this)
        val userd = dbHandler!!.getAllDates().replace("\n", " ")

        var month: Int = 2
        var day: Int = 1
        var year: Int = 3
        val lstValues: List<String> = userd.split("/", " ").map { it -> it.trim() }
        val checkValues: List<String> = userd.split(" ").map { it -> it.trim() }

        for (i in 1..checkValues.lastIndex) {

            calendar.set(lstValues[year].toInt(), lstValues[month].toInt() - 1, lstValues[day].toInt())
            indicators += MainActivity.CalendarDateIndicator(
                eventName = "Indicator #1",
                date = CalendarDate(calendar.time),
                color = calendar_view.getColorInt(R.color.colorPrimaryDark)
            )
            year += 3
            month += 3
            day += 3

        }
        calendar_view.datesIndicators = indicators
        indicators.clear()
    }


    fun res() {
        Toast.makeText(this@MainActivity, "Long tap to change date", Toast.LENGTH_LONG).show()
        val calendarView = findViewById<CalendarView>(R.id.calendar_view)
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.getInstance().get(2), 1)
        val minDate = CalendarDate(calendar.time)
// Initial date
//        println(PrefUtil.getTimerLength(this))
        calendar.set(2019, Calendar.getInstance().get(2), 1)
        var initialDate = CalendarDate(calendar.time)
        if (yearFrom != null) {
            calendar.set(yearFrom!!, monthFrom!!, dayFrom!!)
            initialDate = CalendarDate(calendar.time)
        } else {
            initialDate = CalendarDate(calendar.time)
        }


// Minimum available date
        val datee: MutableList<CalendarDate> = mutableListOf()
        calendar.add(Calendar.DAY_OF_MONTH, -2)
        for (g in 1..100) {
            if (g % 2 == 1) {
                calendar.add(Calendar.DAY_OF_MONTH, 3)
                datee.add(CalendarDate(calendar.time))
                calendar.add(Calendar.DAY_OF_MONTH, 1)
                datee.add(CalendarDate(calendar.time))
            }

        }
        val workDays = dbHandlerWork!!.getAllWorkDays().split("\n")
        val workDaysList = mutableListOf<CalendarDate>()
        for (i in workDays) {
            try {
                val y = i.split("/")
                val yr = y[2].toInt()
                val mo = y[1].toInt()
                val da = y[0].toInt()
                calendar.set(yr, mo - 1, da)
                val x = CalendarDate(calendar.time)
                if (x !in workDaysList) {
                    workDaysList.add(x)
                }
                for (u in workDays) {
                    if (x in datee) {

                    } else {
                        datee.add(CalendarDate(calendar.time))
                    }
                }

            } catch (IndexOutOfBoundsException: IndexOutOfBoundsException) {
                println("End")
            }

        }




// Maximum available date
        calendar.set(Calendar.getInstance().get(1) + 1, Calendar.getInstance().get(2) + 1, 1)
        val maxDate = CalendarDate(calendar.time)


// List of preselected dates that will be initially selected

// The first day of week
        val firstDayOfWeek = java.util.Calendar.MONDAY

// Set up calendar with all available parameters

        calendarView.setupCalendar(
            initialDate = initialDate,
            minDate = minDate,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.MULTIPLE,
            selectedDates = datee,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = false
        )
        val lg=PrefUtil.getTimerLength(this)
        if (lg=="1"){
            changeLang("en")
        }else if(lg=="2"){changeLang("ru")}

    }


    fun resFrom(year: Int = yearFrom!!, month: Int = monthFrom!!, day: Int = dayFrom!!) {
        val calendarView = findViewById<CalendarView>(R.id.calendar_view)
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.getInstance().get(2), 1)
        val minDate = CalendarDate(calendar.time)
// Initial date

        calendar.set(year, month, day)
        val initialDate = CalendarDate(calendar.time)

// Minimum available date

        val dates: MutableList<CalendarDate> = mutableListOf()
        calendar.add(Calendar.DAY_OF_MONTH, -2)
        for (g in 1..100) {
            if (g % 2 == 1) {
                calendar.add(Calendar.DAY_OF_MONTH, 3)
                dates.add(CalendarDate(calendar.time))
                calendar.add(Calendar.DAY_OF_MONTH, 1)
                dates.add(CalendarDate(calendar.time))
            }

        }
        val workDays = dbHandlerWork!!.getAllWorkDays().split("\n")

        val workDaysList = mutableListOf<CalendarDate>()
        for (i in workDays) {
            try {
                val y = i.split("/")
                val yr = y[2].toInt()
                val mo = y[1].toInt()
                val da = y[0].toInt()
                calendar.set(yr, mo - 1, da)
                val x = CalendarDate(calendar.time)
                if (x !in workDaysList) {
                    workDaysList.add(x)
                }
                for (u in workDays) {
                    try {
                        if (x in datee!!) {

                        } else {
                            datee!!.add(CalendarDate(calendar.time))
                        }
                    } catch (Null: NullPointerException) {

                    }
                }

            } catch (IndexOutOfBoundsException: IndexOutOfBoundsException) {
                println("End")
            }

        }

        if (datee != null) {
            for (i in datee!!) {
                if (i in dates) {
                } else {
                    dates.add(i)
                }


            }
        }
        var preselectedDates: List<CalendarDate> = dates


// Maximum available date
        calendar.set(year + 1, month, day)
        val maxDate = CalendarDate(calendar.time)


// List of preselected dates that will be initially selected

// The first day of week
        val firstDayOfWeek = java.util.Calendar.MONDAY

// Set up calendar with all available parameters

        calendarView.setupCalendar(
            initialDate = initialDate,
            minDate = minDate,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.MULTIPLE,
            selectedDates = preselectedDates,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = false
        )


    }

    fun notification(date: String) {
        val notifyID = 1
        val CHANNEL_ID = "my_channel_01"// The id of the channel.
        val name = getString(R.string.channel_name)// The user-visible name of the channel.


        val notification = NotificationCompat.Builder(this)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Ccalendar")
            .setContentText(getString(R.string.notif))
            .setChannelId(CHANNEL_ID).build()


        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mNotificationManager.createNotificationChannel(mChannel)
        }
        mNotificationManager.notify(notifyID, notification)
    }

    fun changeLang(lang: String) {
        val config = baseContext.resources.configuration
        if ("" != lang && !config.locale.language.equals(lang)) {

            val locale = Locale(lang)
            Locale.setDefault(locale)
            val conf = Configuration(config)
            conf.locale = locale
            baseContext.resources.updateConfiguration(conf, baseContext.resources.displayMetrics)
        }
    }

}


